﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChronoBehaviour : MonoBehaviour {

    private GameManager gameManager;
    private Image clockImage;

    private bool isPlayerTurn = true;


	// Use this for initialization
    void Awake(){
        gameManager = GameObject.Find("/GameManager").GetComponent<GameManager>();
        clockImage = GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {

        if (clockImage.enabled && !gameManager.isPlayerTurn)
            clockImage.enabled = false;
        else if (!clockImage.enabled && gameManager.isPlayerTurn)
            clockImage.enabled = true;

        clockImage.fillAmount = gameManager.remainingTime / gameManager.timePerRound;
        clockImage.color = Color.Lerp(Color.red, Color.green, clockImage.fillAmount);
		
	}
}
