﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterBehavior : MonoBehaviour {
    
    public int id;
    public Transform imageTransform;
    public Transform textTransform;

    private GameManager gameManager;
    private Image image;
    private Text text;
    private MeshRenderer meshRenderer;
    private MonsterStat stat;


    private void Awake(){
        gameManager = GameObject.Find("/GameManager").GetComponent<GameManager>();
        image = imageTransform.GetComponent<Image>();
        text = textTransform.GetComponent<Text>();
        meshRenderer = GetComponent<MeshRenderer>();
    }

    private void Update(){
        if (GameManager.socketManager.isConnected)
        {
            stat = gameManager.currentFight[id];


            if (stat.pv <= 0)
            {
                image.color = Color.clear;
                text.text = "";
                meshRenderer.enabled = false;
            } else {
                image.fillAmount = (stat.pv * 1f) / stat.pvMax;
                image.color = Color.Lerp(Color.red, Color.green, (stat.pv * 1f) / stat.pvMax);
                text.text = stat.name;
                meshRenderer.enabled = true;
            }
        }
        
    }
}
