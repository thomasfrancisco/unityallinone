﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using Quobject.SocketIoClientDotNet.Client;

public class SocketManager : MonoBehaviour {
	public bool isConnected = false;
	public float socketTickInSec;
	private Socket socket;
	public JSONNode node;

	public void Start(){
		socket = IO.Socket ("http://localhost:3000");



		//When a the server is connected
		socket.On (Socket.EVENT_CONNECT, () => {
			socket.Emit("ServerConnection", "");

		});

		//When server receive GameState
		socket.On ("GameState", (data) => {
            string json = data.ToString();
            node = JSON.Parse(json);
			if(!isConnected)
				isConnected = true;


		});

		//Launch the coroutine
		StartCoroutine(AskGameState());

	}

	private IEnumerator AskGameState(){
		yield return new WaitForSecondsRealtime (socketTickInSec);
		socket.Emit ("AskGameState", "");
		StartCoroutine (AskGameState ());

	}

	private void OnApplicationQuit(){
		socket.Disconnect ();
	}



		

}
