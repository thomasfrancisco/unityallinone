﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBubbleManager : MonoBehaviour {

    public Transform anchorMin;
    public Transform anchorMax;
    public GameObject playerBubble;

    private GameManager gameManager;
    public Transform listPlayers;

    private List<Transform> bubbles = new List<Transform>();
    private List<TextMesh> textMeshes = new List<TextMesh>();
    private List<PlayerStat> playerStats = new List<PlayerStat>();
    private List<MovingActor> playerMover = new List<MovingActor>();
    public bool isFight;

    void Awake(){
        gameManager = GameObject.Find("/GameManager").GetComponent<GameManager>();
    }
	
	// Update is called once per frame
	void Update () {
        if (GameManager.socketManager.isConnected)
        {

            for (int i = 0; i < listPlayers.childCount; i++)
            {
                if (bubbles.Count <= i)
                {
                    //Add new bubble if it didn't exit;
                    GameObject go = Instantiate(playerBubble, transform);
                    textMeshes.Add(go.transform.GetChild(0).GetComponent<TextMesh>());
                    playerStats.Add(listPlayers.GetChild(i).GetComponent<PlayerStat>());
                    textMeshes[i].text = playerStats[i].pseudo[0].ToString().ToUpper();
                    playerMover.Add(go.GetComponent<MovingActor>());
                    bubbles.Add(go.transform);
                }

                //Move bubble to its position
                playerMover[i].NewPosition(Vector3.Lerp(anchorMin.position, anchorMax.position, (i + 1f) / (listPlayers.childCount + 1f)));
                textMeshes[i].text = playerStats[i].pseudo[0].ToString().ToUpper();

                //If we are in a fight, we disable players that are not in the fight
                if (isFight)
                {

                    //If gameobject active but player not in fight
                    SetActiveObjectOnCondition(bubbles[i], bubbles[i].gameObject.activeSelf && !gameManager.fightParticipant.Contains(listPlayers.GetChild(i).name), false);

                    //If gameobject not active but player in fight
                    SetActiveObjectOnCondition(bubbles[i], !bubbles[i].gameObject.activeSelf && gameManager.fightParticipant.Contains(listPlayers.GetChild(i).name), true);


                } else {
                    // we  disable those who are death or not registered
                    SetActiveObjectOnCondition(bubbles[i], bubbles[i].gameObject.activeSelf && (gameManager.listPlayers[listPlayers.GetChild(i).name].pv <= 0 ||  !gameManager.listPlayers[listPlayers.GetChild(i).name].isRegistered), false);

                    // We activate those who are alive or registered
                    SetActiveObjectOnCondition(bubbles[i], !bubbles[i].gameObject.activeSelf && (gameManager.listPlayers[listPlayers.GetChild(i).name].pv > 0 && gameManager.listPlayers[listPlayers.GetChild(i).name].isRegistered), true);


                }
            }
        }
	}

    //Turn on or off object based on condition
    private void SetActiveObjectOnCondition(Transform bubble, bool condition, bool value){
        if(condition){
            bubble.gameObject.SetActive(value);
        }
    }
}
