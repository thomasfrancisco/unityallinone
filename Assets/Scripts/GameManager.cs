﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static SocketManager socketManager;
	public GameObject playerPrefab;

	private Dictionary<string, Transform> screens = new Dictionary<string, Transform>();
	private Transform currentTransform;

	public Transform[] screenTransform;
	public Transform listPlayersTransform;

	public string leader;
	public string gameState;
	public string[] nextStep = new string[3];
	public Dictionary<string, PlayerStat> listPlayers = new Dictionary<string, PlayerStat>();

    public static ObjectStat[] itemList;
    public static Dictionary<string, ObjectStat> itemByName = new Dictionary<string, ObjectStat>();
    public static MonsterStat[] monsterList;

    public List<MonsterStat> currentFight = new List<MonsterStat>();
    public List<FightAction> fightActions = new List<FightAction>();

    public List<FightAction>[] fightActionQueue = new List<FightAction>[3];

    public List<string> fightParticipant;

    public float timePerRound = 10f;
    public float remainingTime = 10f;

    public bool isPlayerTurn = true;

    public List<Loot> loot = new List<Loot>();

    private bool hasBeenInitialized = false;



	public void Awake(){
		socketManager = gameObject.GetComponent<SocketManager> ();
		for (int i = 0; i < screenTransform.Length; i++) {
			screens.Add (screenTransform [i].name, screenTransform [i]);
		}
		currentTransform = this.transform;
	
	}

    // Update is called once per frame
    void Update()
    {
        if (socketManager.isConnected)
        {
            //Once we've got the first package, we load the constants
            if (!hasBeenInitialized)
            {
                InitializeConstant();
            }


            TranslateGameManager();



            ManageScreen();

        }

    }

	//Interpret value from socketManager.node
    /*
     * It's too repetitive. Need to find a way to factorize (maybe with struct or better class)
     * 
     */
	private void TranslateGameManager(){
        JSONNode node = socketManager.node;

        //ListPlayers
        for (int i = 0; i < node["listPlayers"].Count; i++)
        {
                if (!listPlayers.ContainsKey(node["listPlayers"][i]["ip"].Value))
                {
                    GameObject go = Instantiate(playerPrefab, listPlayersTransform);
                    go.name = node["listPlayers"][i]["ip"].Value;
                    //Add playerStat to dictionnary entry
                    listPlayers.Add(node["listPlayers"][i]["ip"].Value, go.GetComponent<PlayerStat>());
                }
                listPlayers[node["listPlayers"][i]["ip"].Value].UpdatePlayer(node["listPlayers"][i]);
        }

		leader = node ["leader"].Value;
		gameState = node ["gameState"].Value;
		for(int i = 0; i < nextStep.Length; i++){
			nextStep [i] = node ["nextStep"] [i].Value;
		}

        //Current Fight
        for (int i = 0; i < node["currentFight"].Count; i++){

            if(currentFight.Count <= i){
                currentFight.Add(new MonsterStat());
            }
            currentFight[i].name = node["currentFight"][i]["name"];
            currentFight[i].pv = node["currentFight"][i]["pv"];
            currentFight[i].strength = node["currentFight"][i]["strength"];
            currentFight[i].pvMax = node["currentFight"][i]["pvMax"];
        }

        //Fight Actions
        //Erase unnecessary cell
        if(node["fightAction"].Count < fightActions.Count){
            fightActions.Clear();
        }
        //Now we update
        for (int i = 0; i < node["fightAction"].Count;i++){
            if(i >= fightActions.Count){
                fightActions.Add(new FightAction());
            }
            fightActions[i].id = node["fightAction"][i]["id"];
            fightActions[i].side = node["fightAction"][i]["side"];
            fightActions[i].target = node["fightAction"][i]["target"];
        }



        //Fight Participant
        //Erase unnecessaryCell
        if(node["fightParticipant"].Count < fightParticipant.Count){
            fightParticipant.Clear();
        }
        //Now we update
        for (int i = 0; i < node["fightParticipant"].Count;i++){
            if(!fightParticipant.Contains(node["fightParticipant"][i])){
                fightParticipant.Add(node["fightParticipant"][i]);
            }
        }


        if(node["loot"].Count < loot.Count){
            loot.Clear();
        }
        for (int i = 0; i < node["loot"].Count; i++)
        {
            if(i >= loot.Count){
                loot.Add(new Loot());
            }
            loot[i].id = node["loot"][i]["id"].AsInt;
            loot[i].receiver = node["loot"][i]["receiver"].Value;
        }


        remainingTime = node["remainingTime"].AsFloat;
        isPlayerTurn = node["isPlayerTurn"].AsBool;

	}

	//Manage screen according to game state
	private void ManageScreen(){
		if (currentTransform.name != gameState) {
            Debug.Log("Switch screen : " + gameState);
			for (int i = 0; i < screenTransform.Length; i++) {
				screenTransform [i].gameObject.SetActive (false);
			}
			screens [gameState].gameObject.SetActive (true);
            currentTransform = screens[gameState];
		}
	}


    //Initialize object and monster array (won't change during a game)
    private void InitializeConstant(){
        JSONNode node = socketManager.node;
        itemList = new ObjectStat[node["objectList"].Count];
        itemByName.Add(" ", null); //Add the item zero
        monsterList = new MonsterStat[node["monsterList"].Count];
        for (int i = 0; i < node["objectList"].Count; i++) {
            itemList[i] = new ObjectStat(node["objectList"][i]);

            itemByName.Add(itemList[i].name, itemList[i]);
        }
        for (int i = 0; i < node["monsterList"].Count;i++){
            monsterList[i] = new MonsterStat(node["monsterList"][i]);
        }
        timePerRound = node["timePerRound"].AsFloat;

        hasBeenInitialized = true;
        
    }
 
	
}
