﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class MonsterStat {
    public string name;
    public int pv;
    public int pvMax;
    public int strength;

    public MonsterStat(JSONNode monsterNode){
        name = monsterNode["name"].Value;
        pv = monsterNode["pv"].AsInt;
        strength = monsterNode["strength"].AsInt;
        pvMax = monsterNode["pvMax"].AsInt;
    }

    public MonsterStat(){
        name = "Unknown";
        pv = 5;
        strength = 5;
        pvMax = 5;
    }



}
