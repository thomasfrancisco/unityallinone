﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public enum ObjectType {
	head,
	body,
	oneHand,
	twoHand,
	foot,
	extra
};

public class ObjectStat {
	public string name;
	public ObjectType type;
	public int pv;
	public int pm;
	public int strength;
	public int magicka;
	public int speed;

    public ObjectStat(string _name, ObjectType _type, int _pv, int _pm, int _strength, int _magicka, int _speed){
        name = _name;
        type = _type;
        pv = _pv;
        pm = _pm;
        strength = _strength;
        magicka = _magicka;
        speed = _speed;
    }

    public ObjectStat(){
        name = "Unknown";
        type = ObjectType.extra;
        pv = 5;
        pm = 5;
        strength = 5;
        magicka = 5;
        speed = 5;
    }



    public ObjectStat(JSONNode objectNode){
        name = objectNode["name"].Value;
        switch(objectNode["type"].Value){
            case "head":
                type = ObjectType.head;
                break;
            case "body":
                type = ObjectType.body;
                break;
            case "oneHand":
                type = ObjectType.oneHand;
                break;
            case "twoHand":
                type = ObjectType.twoHand;
                break;
            case "foot":
                type = ObjectType.foot;
                break;
            case "extra":
                type = ObjectType.extra;
                break;
            default:
                type = ObjectType.body;
                break;
        }
        pv = objectNode["pv"].AsInt;
        pm = objectNode["pm"].AsInt;
        strength = objectNode["strength"].AsInt;
        magicka = objectNode["magicka"].AsInt;
        speed = objectNode["speed"].AsInt;
    }

    override public string ToString(){
        return ("" + name + " : " + type + " , " + pv + ", " + pm + ", " + strength + ", " + magicka + ", " + speed);
    }

}
