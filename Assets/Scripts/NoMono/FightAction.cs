﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class FightAction {
    public string id;
    public string side;
    public string target;

    public FightAction(string _id, string _side, string _target){
        id = _id;
        side = _side;
        target = _target;
    }

    public FightAction(){
        id = "Unknown";
        side = "Unknown";
        target = "Unknown";

    }

    public FightAction(JSONNode node){
        id = node["attacking"].Value;
        side = node["side"].Value;
        target = node["target"].Value;
    }

}

public class Loot {
    public int id;
    public string receiver;

    public Loot(){
        id = -1;
        receiver = "Unknown";
    }
}

public static class Utilities{
    public static bool CompareVector3(Vector3 v1, Vector3 v2){
        return Vector3.SqrMagnitude(v1 - v2) < 0.01;
    }
}