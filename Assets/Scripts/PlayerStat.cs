﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class PlayerStat : MonoBehaviour {

	public string ip;
	public bool isRegistered;
	public string pseudo;
	public Color color;
	public bool isConnected;
	public int pv;
	public int pvMax;
	public int pm;
	public int pmMax;
	public int strength;
	public int magicka;
	public int speed;
	public string socketId;
	public string vote;
	public ObjectStat head;
	public ObjectStat leftHand;
	public ObjectStat rightHand;
	public ObjectStat body;
	public ObjectStat foot;
	public ObjectStat extra;




    public void UpdatePlayer(JSONNode playerNode){

        ip = playerNode["ip"].Value;
        pseudo = playerNode["name"].Value + " ";
        isRegistered = playerNode["isRegistered"].AsBool;
        isConnected = playerNode["isConnected"].AsBool;
        pv = playerNode["pv"].AsInt;
        pvMax = playerNode["pvMax"].AsInt;
        pm = playerNode["pm"].AsInt;
        pmMax = playerNode["pmMax"].AsInt;
        strength = playerNode["strength"].AsInt;
        magicka = playerNode["magicka"].AsInt;
        speed = playerNode["speed"].AsInt;
        socketId = playerNode["socketId"].Value;
        vote = playerNode["vote"].Value;



    }

    override public string ToString(){
        string ret = "";
        ret += ip + " : " + pseudo + ", " + pv + ", " + pm + ", " + strength + ", " + magicka + ", " + speed + "\n";
        ret += head + "\n";
        ret += leftHand + "\n";
        ret += rightHand + "\n";
        ret += body + "\n";
        ret += foot + "\n";
        ret += extra + "\n";
        return ret;
    }

}
