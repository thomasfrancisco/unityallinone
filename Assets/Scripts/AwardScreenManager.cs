﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AwardScreenManager : MonoBehaviour {

    public GameObject playerBubblePrefab;
    public GameObject itemPrefab;
    public Transform listPlayers;
    public float radius;
    public Vector3 margin;

    private GameManager gameManager;
    private List<Transform> pivot = new List<Transform>(); //Contains players bubble transform. Also use as pivot
    private List<Transform> items = new List<Transform>(); //Contains item transforms
    private List<Image> images = new List<Image>(); //Contains reference to images
    private List<Text> texts = new List<Text>(); //Contains reference to texts

    private List<MovingActor> playerMover = new List<MovingActor>();
    private List<MovingActor> itemMover = new List<MovingActor>();

    void Awake(){
        gameManager = transform.Find("/GameManager").GetComponent<GameManager>();
    }

    void Start(){
        
    }
	
	// Update is called once per frame
	void Update () {
        if(GameManager.socketManager.isConnected){


            //First we create the loot object
            for (int i = 0; i < gameManager.loot.Count; i++)
            {
                if (items.Count <= i)
                {
                    GameObject go = Instantiate(itemPrefab);
                    go.name = GameManager.itemList[gameManager.loot[i].id].name;
                    items.Add(go.transform);
                    images.Add(go.transform.GetChild(0).GetChild(0).GetComponent<Image>());
                    texts.Add(go.transform.GetChild(0).GetChild(1).GetComponent<Text>());
                    itemMover.Add(go.GetComponent<MovingActor>());
                }

                texts[i].text = GameManager.itemList[gameManager.loot[i].id].name;
            }

            //Remove useless object
            for (int i = items.Count; i < gameManager.loot.Count; i++)
            {
                images.RemoveAt(i);
                texts.RemoveAt(i);
                itemMover.RemoveAt(i);
                Destroy(items[i]);
                items.RemoveAt(i);
            }
            //Add player Bubble
            for (int i = 0; i < listPlayers.childCount; i++){
                if (pivot.Count <= i) {
                    GameObject go = Instantiate(playerBubblePrefab, transform);
                    go.name = gameManager.listPlayersTransform.GetChild(i).name;
                    go.transform.GetChild(0).GetComponent<TextMesh>().text = listPlayers.GetChild(i).GetComponent<PlayerStat>().pseudo[0].ToString().ToUpper();
                    pivot.Add(go.transform);
                    playerMover.Add(go.GetComponent<MovingActor>());

                }

                playerMover[i].NewPosition(new Vector3(radius * Mathf.Cos(i * (2 * Mathf.PI / listPlayers.childCount)), radius * Mathf.Sin(i * (2 * Mathf.PI / listPlayers.childCount)), 0));
                //If player not in fight and his bubble is active
                if(!gameManager.fightParticipant.Contains(listPlayers.GetChild(i).name) && pivot[i].gameObject.activeSelf){
                    pivot[i].gameObject.SetActive(true);
                }

                if(gameManager.fightParticipant.Contains(listPlayers.GetChild(i).name) && !pivot[i].gameObject.activeSelf){
                    pivot[i].gameObject.SetActive(false);
                }

                //Iterate through loot to give player his object
                for (int j = 0; j < gameManager.loot.Count; j++)
                {
                    if (gameManager.loot[j].receiver == listPlayers.GetChild(i).name)
                    {
                        items[j].SetParent(pivot[i]);
                        itemMover[j].NewPosition(items[j].parent.position * 2 + (Vector3.down * margin.x * (items[j].GetSiblingIndex() -1 )));
                    }
                }

            }

        }


		
	}
}
