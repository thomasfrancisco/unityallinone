﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelManager : MonoBehaviour {
    private string[] mapType = { "Hospital", "Marketplace", "Spring", "Fight" };


    private GameManager gameManager;
    private List<TextMesh> textMeshs = new List<TextMesh>();

    private void Awake(){
        for (int i = 0; i < transform.childCount; i++){
            textMeshs.Add(transform.GetChild(i).GetComponent<TextMesh>());
        }

        gameManager = transform.Find("/GameManager").GetComponent<GameManager>();
    }

    private void Update(){
        if (GameManager.socketManager.isConnected)
        {
            string[] labels = TranslatePath(gameManager.nextStep);
            for (int i = 0; i < textMeshs.Count; i++)
            {
                textMeshs[i].text = labels[i];
            }
        }
        
    }


    //Translate path information sent as int. Convert it to string array with info about path
    private string[] TranslatePath(string [] path){
        string[] ret = new string[path.Length];
        for (int i = 0; i < ret.Length; i++){
            ret[i] = mapType[int.Parse(path[i][0].ToString())];
        }
        return ret;
    }
}
