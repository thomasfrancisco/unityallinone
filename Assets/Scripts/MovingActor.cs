﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingActor : MonoBehaviour {

    private Vector3 lastPosition = new Vector3(0f, 0f, 0f);
    private float speed = 5f;
    private Queue<Vector3[]> movementQueue = new Queue<Vector3[]>();
    private bool isMoving = false;

    public void NewPosition(Vector3 position){
        if(!Utilities.CompareVector3(position, lastPosition)){
            Debug.Log(Time.time + " ::: "+ transform.name + " ::: " + "Enqueu " + lastPosition + " to " + position);
            movementQueue.Enqueue(new Vector3[]{
                lastPosition, position
            });
            if (!isMoving)
            {
                Debug.Log(Time.time + " ::: " + transform.name + " ::: " + " Dequeu in NewPosition " + movementQueue.Peek()[0] + " to " + movementQueue.Peek()[1]);
                StartCoroutine(Goto(movementQueue.Peek()[0], movementQueue.Peek()[1], 1f));
                movementQueue.Dequeue();
            } else {
                Debug.Log(transform.name + " is moving");
            }
        }
        lastPosition = position;
    }

    private void Update(){
        /*
        if (!isMoving && movementQueue.Count > 0)
        {
            Debug.Log(Time.time + " ::: " + transform.name + " ::: " + "Dequeue in update " + movementQueue.Peek()[0] + " to " + movementQueue.Peek()[0]);
            StartCoroutine(Goto(movementQueue.Peek()[0], movementQueue.Peek()[1]));
            movementQueue.Dequeue();
        }*/
    }

    private IEnumerator Goto(Vector3 from, Vector3 target, float timeInSeconds){
        isMoving = true;
        float alpha = 0f;
        while(alpha < 1f){
            transform.position = Vector3.Lerp(from, target, alpha);
            alpha += timeInSeconds * Time.deltaTime;
            Debug.Log(transform.name + " ::: " + alpha);
            yield return new WaitForEndOfFrame();
        }
        if(movementQueue.Count != 0){
            Debug.Log(Time.time + " ::: " + transform.name + " ::: " + "Dequeu in coroutine " + movementQueue.Peek()[0] + " to " + movementQueue.Peek()[1]);
            StartCoroutine(Goto(movementQueue.Peek()[0], movementQueue.Peek()[1], 1f));
            movementQueue.Dequeue();
        } else {
            isMoving = false;
        }
    }
}
